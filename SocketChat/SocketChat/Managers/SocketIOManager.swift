//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by mac-8 on 14.02.17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

import Foundation
import SocketIO

class SocketIOManager {
    
    static let shared: SocketIOManager = {
        let instance = SocketIOManager()
        return instance
    }()
    
    // ifconfig in terminal to see that info
    // node index.js in terminal
    // 192.168.1.8 address in wifi net
    // 3000 port to connect
    var socket = SocketIOClient(socketURL: URL(string: "http://192.168.1.8:3000")!)
    
    //MARK: - Connection setup
    
    func establishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func connectToServerWithNickname(nickname: String, completion:@escaping (_ userList: [[String:AnyObject]]?)->()) {
        // sending any message to the server
        socket.emit("connectUser", nickname)
        
        // listen to the socket for any message regarding the user list
        // The first argument we have to specify is the literal of the message we are interested in
        // The second argument is a closure with two parameters. The first one is a NSArray object, where the number of the contained elements depends on the number of results sent by the server. Always remember that the first parameter in that closure is an array, even if the server returns a single value back. It will exist in the first index of the array. The second parameter is an acknowledge message that can be used to notify the server that the message has been received. Note that the naming properly the above parameters is up to you, meaning that the dataArray and ack are just names that I chose here.
        socket.on("userList") { (dataArray, ack) in
            completion(dataArray[0] as? [[String:AnyObject]])
        }
        
        // Tracking users statuses
        listenForOtherMessages()
    }
    
    func exitChatWithNockname(nickname: String, completion:()->()) {
        socket.emit("exitUser", nickname)
        completion()
    }
    
    //MARK: - Chating
    
    func sendMessage(message: String, withNickname nickname: String) {
        socket.emit("chatMessage", nickname, message)
    }
    
    func getChatMessage(completion:@escaping (_ messageInfo: [String:String])->()) {
        socket.on("newChatMessage") { (dataArray, ack) in
            var messageDictionary = [String:String]()
            messageDictionary["nickname"] = dataArray[0] as? String
            messageDictionary["message"] = dataArray[1] as? String
            messageDictionary["date"] = dataArray[2] as? String
            completion(messageDictionary)
        }
    }
    
    //MARK: - Notifications of user activity
    
    private func listenForOtherMessages() {
        socket.on("userConnectUpdate") { (dataArray, ack) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0])
        }
        
        socket.on("userExitUpdate") { (dataArray, ack) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: dataArray[0])
        }
        
        socket.on("userTypingUpdate") { (dataArray, ack) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: dataArray[0])
        }
    }
    
    func sendStartTypingMessage(nickname: String) {
        socket.emit("startType", nickname)
    }
    
    func sendStopTypingMessage(nickname: String) {
        socket.emit("stopType", nickname)
    }
}
